#include "main.h"
#include "dllist.h"

#include "utility.h"
#include "sysdep.h"

// 입력한 수(N) 만큼의 노드를 생성
void
InsertToDLList(DLList* list, int n, int which)
{
	void* item;
	int key;

	for (int i = 0; i != n; i++)
	{
		// key값은 정수 0 ~ 99 중 랜덤으로 설정 (= rand() % 100)
		item = new int(RandomNumber());
		key = (int) item % 100;
		list->SortedInsert(item, key);
		cout << "Thread [" << which << "] : Item [" << item << "] was inserted with key [" << key << "]\n";
		
		// interrupt flag 2
		if (flag == 2)
		{
			cout << "*** Insert to DLList interrupt\n";
			kernel->currentThread->Yield();
		}
	}
}

// 입력한 수(N) 만큼의 노드를 제거
void
RemoveToDLList(DLList* list, int n, int which)
{
	void* item;
	int key;
	int i = 0;

	while (!(list->IsEmpty()) && i != n)
	{
		if ((item = list->Remove(&key)))
		{
			cout << "Thread [" << which << "] : Item [" << item << "] was removed with key [" << key << "]\n";
		}
		i++;
		
		// interrupt flag 3
		if (flag == 3)
		{
			cout << "*** Remove to DLList interrupt\n";
			kernel->currentThread->Yield();
		}
	}
}
