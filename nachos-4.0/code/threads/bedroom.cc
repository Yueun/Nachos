#include "bedroom.h"
#include "utility.h"
#include "main.h"

Bedroom::Bedroom()
{
    currentInterrupt = 0;
}

// WaitUntil이 호출되면 list에 넣고 Sleep
void
Bedroom::PutToBed(Thread* t, int x)
{
    ASSERT(kernel->interrupt->getLevel() == IntOff);
    beds.push_back(Bed(t, currentInterrupt + x));
    t->Sleep(false);
}

// MorningCall이 호출되면 list를 순회해 Thread를 준비상태로 만들고,
// 동작 결과를 반환함
bool
Bedroom::MorningCall()
{
    bool woken = false;

    currentInterrupt++;

    for (std::list<Bed>::iterator it = beds.begin(); it != beds.end(); )
    {
        if (currentInterrupt >= it->when)
        {
            woken = true;
            cout << "Bedroom::MorningCall" << endl;
            kernel->scheduler->ReadyToRun(it->sleeper);
            it = beds.erase(it);
        } else {
            it++;
        }
    }

    return woken;
}

// list가 비었는지 아닌지 판단
bool
Bedroom::IsEmpty()
{
    return beds.size() == 0;
}
