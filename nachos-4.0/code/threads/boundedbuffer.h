#ifndef BOUNDEDBUFFER_H
#define BOUNDEDBUFFER_H

#include "list.h"
#include "synch.h"

class BoundedBuffer
{
public:
    BoundedBuffer(int);
    ~BoundedBuffer();

    void Read(void*, int);
    void Write(void*, int);

private:
    char* buffer; // 버퍼는 Circular Queue로 구현
    int in, out; // input, output index
    int count; // 현재 데이터 수
    int capacity; // 최대 데이터 수

    // Empty와 Full 상태를 나타내줄 두 개의 Lock과
    // 현재 버퍼의 상태를 나타내 줄 Condition 사용
    Lock* notEmpty;
    Lock* notFull;
    Condition* condition;
};

#endif // BOUNDEDBUFFER_H
