#include "main.h"
#include "dllist.h"

#include "utility.h"
#include "sysdep.h"

DLLElement::DLLElement(void* itemPtr, int sortKey)
{
	// 노드의 앞과 뒤를 NULL로 초기화
	next = NULL;
	prev = NULL;

	item = itemPtr;
	key = sortKey;
}

DLList::DLList()
{
	// 리스트의 처음과 끝을 NULL로 초기화
	first = NULL;
	last = NULL;
}

// 리스트가 비었다면 TRUE, 그렇지 않다면 FALSE를 반환
bool
DLList::IsEmpty()
{
	if (first == NULL)
	{
		return TRUE;
	}
	return FALSE;
}

// 리스트를 비움
DLList::~DLList()
{
	if (!IsEmpty())
	{
		// 포인터 ptr이 리스트의 처음 노드를 가리키게 하고,
		// 리스트의 처음 노드가 그 다음 노드를 가리키게 한다.
		// 그 다음 ptr을 제거하고 이를 리스트의 끝까지 반복하여 리스트를 비운다
		while (first != last)
		{
			DLLElement* ptr = first;
			first = first->next;
			delete ptr;
		}
		
		delete first;
	}
}

// 리스트의 가장 처음에 새 노드 추가
void
DLList::Prepend(void* item)
{
	if (IsEmpty())
	{
		// 리스트가 비었다면 0번 노드를 생성 후 추가
		DLLElement* ptr = new DLLElement(item, 0);
		last = first = ptr;
	}
	else
	{
		// 그렇지 않다면 처음 노드의 key값 보다 1만큼 작은 key값을 가진 노드 생성
		int key = first->key -1;
		DLLElement* ptr = new DLLElement(item, key);
		
		// 그 후 새 노드의 다음이 처음 노드를 가리키게 하고,
		// 처음 노드의 이전을 새 노드로 바꾼 다음,
		// 리스트의 처음이 새 노드를 가리키게 한다
		ptr->next = first;
		first->prev = ptr;
		first = ptr;
	}
}

// 리스트의 가장 마지막에 새 노드 추가
void
DLList::Append(void* item)
{
	if (IsEmpty())
	{
		DLLElement* ptr = new DLLElement(item, 0);
		first = ptr;
		last = first;
	}
	else
	{
		// 리스트가 비어있지 않다면 리스트의 마지막 노드보다 1만큼 작은 key값을 가진 노드 생성
		int key = last->key + 1;
		DLLElement* ptr = new DLLElement(item, key);

		// 그 후 마지막 노드의 다음이 새 노드를 가리키게 하고,
		// 새 노드의 마지막을 이전 노드로 바꾼 다음,
		// 리스트의 마지막이 새 노드를 가리키게 한다
		last->next = ptr;
		ptr->prev = last;
		last = ptr;
	}
}

// 리스트의 처음 노드를 제거
void*
DLList::Remove(int* keyPtr)
{
	// 리스트가 비었다면 NULL을 반환
	if (IsEmpty())
	{
		keyPtr = NULL;
		return NULL;
	}
	else
	{
		DLLElement* ptr = first;
		void* item = first->item;		
	
		// 그렇지 않다면 keyPtr이 처음 노드의 key값을 가리키게 하고,
		*keyPtr = first->key;

		if (first == last)
		{
			first = NULL;
			last = NULL;
		}
		else
		{
			// 리스트의 처음 노드가 처음 노드가 다음 노드를 가리키게 하고,
			// 이전 노드와의 연결을 제거하여 처음 노드 제거
			first = first->next;
			first->prev = NULL;
		}

		delete ptr;
		return item;
	}
}

// key값을 기준으로 정렬상태를 유지하며 노드 추가
// 이전에 입력되어있던 노드들과 비교하여, 위치를 찾아 오름차순으로 정렬
void
DLList::SortedInsert(void* item, int sortKey)
{
	DLLElement* ptr = new DLLElement(item, sortKey);
	
	if (IsEmpty())
	{
		first = ptr;
		last = ptr;
	}
	else
	{
		if (first->key > sortKey)
		{
			// interrupt flag 4
			if (flag == 4)
			{
				cout << "*** Sorted Insert Interrupt\n";
				kernel->currentThread->Yield();
			}
			
			ptr->next = first;
			first->prev = ptr;
			ptr->prev = NULL;
			first = ptr;
		}
		else
		{
			DLLElement* head = first;
			while (head->next != NULL)
			{
				if (head->next->key > sortKey)
				{
					// interrupt flag 4
					if (flag == 4)
					{
						cout << "*** Sorted Insert Interrupt\n";
						kernel->currentThread->Yield();
					}
					ptr->next = head->next;
					ptr->prev = head;
					head->next->prev = ptr;
					head->next = ptr;
					return;
				}
				else
				{
					head = head->next;
				}
			}
			// interrupt flag 4
			if (flag == 4)
			{
				cout << "*** Sorted Insert Interrupt\n";
				kernel->currentThread->Yield();
			}
			
			head->next = ptr;
			ptr->prev = head;
			ptr->next = NULL;
			last = last->next;
		}
	}
}

// key값이 동일한 노드를 제거
// key값이 동일한 노드를 찾고, 그 다음 노드와 이전 노드의 상태를 저장하여 연결하기 위해 2개의 포인터(head1, head2) 사용
void*
DLList::SortedRemove(int sortKey)
{
	if (IsEmpty())
	{
		return NULL;
	}
	
	DLLElement* head1 = first;
	DLLElement* head2 = first;

	while((head1 != NULL) && (head1->key != sortKey))
	{
		head2 = head1;
		head1 = head1->next;
	}
	if (head1 = NULL)
	{
		return NULL;
	}
	else if (head1 == first)
	{
		DLLElement* ptr = first;
		void* item = first->item;
		first = first->next;

		if (first == NULL)
		{
			first = NULL;
			last = NULL;
			delete ptr;
			return item;
		}
		else
		{
			first->prev = NULL;
			delete ptr;
			return item;
		}
	}
	else
	{
		void* item = head1->item;
		head2->next = head1->next;

		if (head1->next != NULL)
		{
			head1->next->prev = head2;
		}
		else
		{
			last = head2;
		}

		delete head1;
		return item;
	}
}
