#ifndef DLLIST_H
#define DLLIST_H

class DLLElement
{
public:
	DLLElement(void* itemPtr, int sortKey); // 노드 초기화
	
	// 리스트의 노드들을 양방향으로 연결하기 위해서는 각 노드의 다음과 이전을 알 수 있어야 한다
	DLLElement* next;
	DLLElement* prev;

	void* item;
	int key; // 정렬을 위한 key값
};

class DLList
{
public:
	DLList(); // 리스트 초기화
	~DLList(); // 리스트를 비움

	bool IsEmpty(); // 리스트가 비어있는지 아닌지 판단

	void Prepend(void* item); // 리스트의 가장 처음에 새 노드 추가
	void Append(void* item); // 리스트의 가장 마지막에 새 노드 추가
	void* Remove(int* keyPtr); // 리스트의 처음 노드를 제거

	void SortedInsert(void* item, int sortKey); // 정렬상태를 유지하며 노드 추가
	void* SortedRemove(int sortKey); // key값이 동일한 노드 제거

private:
	// 삽입, 삭제, 정렬을 위해서는 리스트의 처음과 끝을 알 수 있어야 한다
	DLLElement* first;
	DLLElement* last;
};

extern int T, N, flag; // T(Thread)와 N(Node)은 각각 생성할 스레드와 노드의 수
		       // 그리고 flag를 통하여 각종 interrupt를 발생시켜 그에 따른 오류를 확인

extern void InsertToDLList(DLList* list, int n, int which);
extern void RemoveToDLList(DLList* list, int n, int which);

#endif // DLLIST_H
