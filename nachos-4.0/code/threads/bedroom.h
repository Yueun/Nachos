#ifndef BEDROOM_H
#define BEDROOM_H

#include "thread.h"
#include <list>

class Bedroom
{
public:
    Bedroom();

    void PutToBed(Thread* t, int x);
    bool MorningCall();
    bool IsEmpty();

private:
    class Bed
    {
        public:
            Bed(Thread* t, int x):
                sleeper(t), when(x) {};
                
            Thread* sleeper;
            int when;
    };

    int currentInterrupt;
    std::list<Bed> beds;
};

#endif // BEDROOM_H

