// main.cc 
//	Driver code to initialize, selftest, and run the 
//	operating system kernel.  
//
// Usage: nachos -u -z -d <debugflags> ...
//   -u prints entire set of legal flags
//   -z prints copyright string
//   -d causes certain debugging messages to be printed (cf. debug.h)
//
//  NOTE: Other flags are defined for each assignment, and
//  incorrect flag usage is not caught.
//
// Copyright (c) 1992-1996 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#define MAIN
#include "copyright.h"
#undef MAIN

#include "main.h"

// global variables
KernelType *kernel;
Debug *debug;

// 생성할 스레드, 노드의 수와 flag를 담을 변수 선언
int T, N, flag;

// 스케줄링 타입을 담을 변수 선언
SchedulerType schedulerType;

//----------------------------------------------------------------------
// Cleanup
//	Delete kernel data structures; called when user hits "ctl-C".
//----------------------------------------------------------------------

static void 
Cleanup(int x) 
{     
    cerr << "\nCleaning up after signal " << x << "\n";
    delete kernel; 
}


//----------------------------------------------------------------------
// main
// 	Bootstrap the operating system kernel.  
//	
//	Initialize kernel data structures
//	Call selftest procedure
//	Run the kernel
//
//	"argc" is the number of command line arguments (including the name
//		of the command) -- ex: "nachos -d +" -> argc = 3 
//	"argv" is an array of strings, one for each command line argument
//		ex: "nachos -d +" -> argv = {"nachos", "-d", "+"}
//----------------------------------------------------------------------

int
main(int argc, char **argv)
{
    int i;
    char *debugArg = "";

	T = N = flag = 0;

	// 스레드, 노드의 수와 flag는 main()의 인자로 받는다
	// T = atoi(argv[1]);
	// N = atoi(argv[2]);
	// flag = atoi(argv[3]);

    // before anything else, initialize the debugging system
    for (i = 1; i < argc; i++) {
        if (strcmp(argv[i], "-d") == 0) {
	    ASSERT(i + 1 < argc);   // next argument is debug string
            debugArg = argv[i + 1];
	    i++;
	} else if (strcmp(argv[i], "-u") == 0) {
            cout << "Partial usage: nachos [-z -d debugFlags]\n";
	} else if (strcmp(argv[i], "-z") == 0) {
            cout << copyright;
	}

    }
    debug = new Debug(debugArg);
    
    DEBUG(dbgThread, "Entering main");

    // 스케줄링 타입은 main()의 인자로 받음
    // schedulerType = RR;
    // 
    // if (strcmp(argv[1], "FCFS") == 0)
    // {
    //     schedulerType = FCFS;
    // }
    // else if (strcmp(argv[1], "SJF") == 0)
    // {
    //     schedulerType = SJF;
    // }
    // else if (strcmp(argv[1], "PRIORITY") == 0)
    // {
    //     schedulerType = Priority;
    // }

    kernel = new KernelType(argc, argv);
    kernel->Initialize();
    // kernel->Initialize(schedulerType);
    
    CallOnUserAbort(Cleanup);		// if user hits ctl-C

    // kernel->SelfTest();

    // HW1Test() 호출
    // kernel->HW1Test();

    // SchedulerTest() 호출
    // kernel->SchedulingTest();

    // BoundedBufferTest() 호출
    kernel->BoundedBufferTest();

    kernel->Run();
    
    return 0;
}
