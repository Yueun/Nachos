#include "boundedbuffer.h"

BoundedBuffer::BoundedBuffer(int maxSize)
{
    buffer = (char*)malloc(maxSize);
    in = out = 0;
    count = 0;
    capacity = maxSize;

    notEmpty = new Lock("notEmpty");
    notFull = new Lock("notFull");
    condition = new Condition("condition");
}

BoundedBuffer::~BoundedBuffer()
{
    delete notEmpty;
    delete notFull;
    delete condition;
    free(buffer);
}

void
BoundedBuffer::Read(void* data, int size)
{
    char* outBuffer = (char*)data;

    while (size--)
    {
        // 동시접근을 제한하기 위해 Lock을 Acquire
        notEmpty->Acquire();

        // 버퍼에 데이터가 없다면 Producer가 데이터를 생산할 때 까지 대기
        while (count == 0)
        {
            condition->Wait(notEmpty);
        }

        notEmpty->Release();
        *(outBuffer++) = buffer[out];
        out = (out + 1) % capacity;

        // 데이터를 소비한 후 count를 줄이기 전에 Lock
        notFull->Acquire();
        --count;

        // count를 줄인 후 가득 차 있지 않다는 신호를 전송하고 Lock을 해제
        condition->Signal(notFull);
        notFull->Release();
    }
}

void
BoundedBuffer::Write(void* data, int size)
{
    char* inBuffer = (char*)data;
    
    while (size--)
    {
        // 동시접근을 제한하기 위해 Lock을 Acquire
        notFull->Acquire();

        // 버퍼에 저장할 공간이 없다면, Consumer가 데이터를 소비할 때 까지 대기
        while (count >= capacity)
        {
            condition->Wait(notFull);
        }

        notFull->Release();
        buffer[in] = *(inBuffer++);
        in = (in + 1) % capacity;

        // 데이터를 생산한 후 count를 늘리기 전에 Lock
        notEmpty->Acquire();
        ++count;

        // count를 줄인 후 비어 있지 않다는 신호를 전송하고 Lock을 해제
        condition->Signal(notEmpty);
        notEmpty->Release();
    }
}
