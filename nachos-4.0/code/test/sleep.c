// Sleep을 테스트하기 위한 코드

#include "syscall.h"

main()
{
    int i;

    for (i = 0; i < 5; i++)
    {
        Sleep(10000);
        PrintInt(i);
    }

    return 0;
}
